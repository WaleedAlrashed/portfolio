import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ExpierencesListPage extends StatefulWidget {
  const ExpierencesListPage({super.key});

  @override
  State<ExpierencesListPage> createState() => _ExpierencesListPageState();
}

class _ExpierencesListPageState extends State<ExpierencesListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(40.0),
        child: AppBar(
          backgroundColor: Color(0x44000000),
          elevation: 0,
          title: Text(
            "Experience",
            style: GoogleFonts.cairo(
              color: Colors.white,
            ),
          ),
        ),
      ),
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Colors.blue,
              Colors.red,
            ],
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 50.0,
            ),
            child: Column(
              children: <Widget>[
                Card(
                  child: ListTile(
                    leading: Icon(
                      Icons.flutter_dash,
                      color: Colors.blueAccent,
                    ), // Added leading icon
                    title: Text('Flutter'),
                    subtitle: Text(
                        'Experience with cross-platform mobile app development.'),
                    trailing: Icon(Icons.chevron_right), // Added trailing icon
                  ),
                ),
                Card(
                  child: ListTile(
                    leading: Icon(
                      Icons.web,
                      color: Colors.red,
                    ), // Added leading icon
                    title: Text('Laravel'),
                    subtitle: Text(
                        'Experience with web application development using PHP.'),
                    trailing: Icon(Icons.chevron_right), // Added trailing icon
                  ),
                ),
                Card(
                  child: ListTile(
                    leading: Icon(
                      Icons.storage,
                      color: Colors.blue,
                    ), // Added leading icon
                    title: Text('MySQL'),
                    subtitle:
                        Text('Experience with database management and SQL.'),
                    trailing: Icon(Icons.chevron_right), // Added trailing icon
                  ),
                ),
                Card(
                  child: ListTile(
                    leading: Icon(
                      Icons.build_circle,
                      color: Colors.orange,
                    ), // Added leading icon
                    title: Text('CI/CD'),
                    subtitle: Text(
                        'Experience with continuous integration and continuous deployment.'),
                    trailing: Icon(Icons.chevron_right), // Added trailing icon
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
