import 'package:flutter/material.dart';

const kDarkGradient = [
  Color.fromRGBO(67, 67, 67, 1),
  Color.fromRGBO(0, 0, 0, 1)
];

const kBlueRed = [
  Colors.blue,
  Colors.red,
];

const kWinterNeva = [
  Color.fromRGBO(161, 196, 253, 1),
  Color.fromRGBO(194, 233, 251, 1),
];
const kGreenish = [
  Color.fromRGBO(155, 255, 93, 1),
Color.fromRGBO(30, 30, 30,0.6),
  Color.fromRGBO(0, 227, 174, 1),
];

const kyellow = [
Color.fromRGBO(3, 3, 3, 1),
Color.fromRGBO(3, 3, 3, 1),
];
// const KAmberWhite = [Colors.amber[300],Colors.white12,];