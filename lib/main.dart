import 'package:animate_do/animate_do.dart';
import 'package:animated_background/animated_background.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:waleed_alrashed/pages/expierences_list_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with TickerProviderStateMixin {
  AnimationController? animationController;

  _launchURL(String url) async {
    print('taped');
    // const url = 'mailto:smith@example.org?subject=News&body=New%20plugin';
    if (await canLaunchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Waleed Alrashed',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(40.0),
          child: AppBar(
            backgroundColor: Color(0x44000000),
            elevation: 0,
            title: Text(
              "Portfolio",
              style: GoogleFonts.cairo(
                color: Colors.white,
              ),
            ),
            actions: [
              Builder(
                builder: (context) => IconButton(
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ExpierencesListPage(),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.list,
                    color: Colors.pink,
                  ),
                ),
              )
            ],
          ),
        ),
        backgroundColor: Color.fromRGBO(17, 34, 51, 1),
        body: AnimatedBackground(
          behaviour: RandomParticleBehaviour(
            options: ParticleOptions(
              spawnMaxSpeed: 10,
              spawnMinSpeed: 5,
              maxOpacity: 0.4,
              minOpacity: 0.0,
              spawnMaxRadius: 15,
              spawnMinRadius: 2,
              baseColor: Colors.blue.shade300,
            ),
          ),
          vsync: this,
          child: Center(
            child: Container(
              child: SafeArea(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Flash(
                      animate: true,
                      manualTrigger: true,
                      controller: (animateController) =>
                          animateController = this.animationController!,
                      child: Tooltip(
                        message: "Oh Yeah that's me 😄",
                        decoration: BoxDecoration(
                          color: Colors.green,
                        ),
                        child: CircleAvatar(
                          radius: 50.0,
                          backgroundImage: AssetImage('images/waleed.jpg'),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Waleed Alrashed',
                      style: GoogleFonts.sniglet(
                        color: Colors.white,
                        fontSize: 40.0,
                      ),
                    ),
                    Text(
                      'Software Developer',
                      style: GoogleFonts.roboto(
                        textStyle: TextStyle(
                          color: Colors.teal.shade100,
                          fontSize: 20.0,
                          letterSpacing: 2.5,
                        ),
                      ),
                      // style: TextStyle(
                      //     fontFamily: 'Source Sans Pro',
                      //     color: Colors.teal.shade100,
                      //     fontSize: 18.0,
                      //     letterSpacing: 2.5,
                      //     fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 20.0,
                      width: 150.0,
                      child: Divider(
                        color: Colors.teal.shade100,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(5),
                      margin: EdgeInsets.only(
                          right: 30, left: 30, top: 30, bottom: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [
                            Colors.white54,
                            Colors.white54,
                          ],
                        ),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Tooltip(
                            message: "Github",
                            child: IconButton(
                              highlightColor: Colors.blueAccent,
                              hoverColor: Colors.blue.shade900,
                              icon: FaIcon(FontAwesomeIcons.github),
                              onPressed: () => _launchURL(
                                  'https://github.com/WaleedAlrashed'),
                              // hoverColor: Colors.blue,
                            ),
                          ),
                          Tooltip(
                            message: "Stackoverflow",
                            child: IconButton(
                                highlightColor: Colors.blueAccent,
                                hoverColor: Colors.blue.shade900,
                                icon: FaIcon(FontAwesomeIcons.stackOverflow),
                                onPressed: () => _launchURL(
                                    'https://stackoverflow.com/users/7567572/waleed-syr')),
                          ),
                          Tooltip(
                            message: "Hire Me 🤩",
                            child: IconButton(
                                highlightColor: Colors.blueAccent,
                                hoverColor: Colors.blue.shade900,
                                icon: Icon(FontAwesomeIcons.linkedin),
                                onPressed: () => _launchURL(
                                    'https://www.linkedin.com/in/waleedalrashed/')),
                          ),
                          // Tooltip(
                          //     message: "Youtube",
                          //     child: IconButton(
                          //         icon: Icon(FontAwesomeIcons.youtubeSquare),
                          //         // iconSize: 15,
                          //         onPressed: () => _launchURL(
                          //             'https://www.youtube.com/channel/UC291e3Q-GBVZ8VsMsNFZEVw'))),
                          Tooltip(
                            message: "Resume",
                            child: IconButton(
                                highlightColor: Colors.blueAccent,
                                hoverColor: Colors.blue.shade900,
                                icon: Icon(FontAwesomeIcons.solidFilePdf),
                                onPressed: () => _launchURL(
                                    'https://waleedalrashed.com/waleedsresume.pdf')),
                          ),
                          Tooltip(
                            message: "Say Hi 😄",
                            child: IconButton(
                              highlightColor: Colors.blueAccent,
                              hoverColor: Colors.blue.shade900,
                              icon: Icon(FontAwesomeIcons.envelope),
                              onPressed: () async {
                                Clipboard.setData(
                                  ClipboardData(text: "wmr121@gmail.com"),
                                );
                                // ClipboardManager.copyToClipBoard(
                                //         "your text to copy")
                                //     .then((result) {
                                //   final snackBar = SnackBar(
                                //     content: Text('Copied to Clipboard'),
                                //     action: SnackBarAction(
                                //       label: 'Undo',
                                //       onPressed: () {},
                                //     ),
                                //   );
                                //   Scaffold.of(context).showSnackBar(snackBar);
                                // });
                                // makeAlert(context);
                                if (kIsWeb) {
                                  // Info.showGo('wmr121@gmail.com');
                                } else {
                                  _launchURL(
                                      'mailto:wmr121@gmail.com?subject=Say Hello&body=Hello Waleed');
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Card buildCard(String title) {
    return Card(
      color: Colors.white,
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
      child: ListTile(
        leading: Icon(
          Icons.email,
          color: Colors.teal,
        ),
        title: Text(
          title,
          style: TextStyle(
            color: Colors.teal.shade900,
            fontFamily: 'Source Sans Pro',
            fontSize: 20.0,
          ),
        ),
      ),
    );
  }

  void makeAlert(BuildContext context) {
    print('gg');
    Alert(
      context: context,
      type: AlertType.warning,
      title: "RFLUTTER ALERT",
      desc: "Flutter is more awesome with RFlutter Alert.",
      buttons: [
        DialogButton(
          child: Text(
            "FLAT",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Color.fromRGBO(0, 179, 134, 1.0),
        ),
        DialogButton(
          child: Text(
            "GRADIENT",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          gradient: LinearGradient(colors: [
            Color.fromRGBO(116, 116, 191, 1.0),
            Color.fromRGBO(52, 138, 199, 1.0)
          ]),
        )
      ],
    ).show();
  }
}
